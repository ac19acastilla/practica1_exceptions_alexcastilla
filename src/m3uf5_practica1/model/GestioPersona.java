package m3uf5_practica1.model;

import java.time.LocalDate;
import static java.time.temporal.ChronoUnit.YEARS;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GestioPersona implements IGestioPersona {

    private List<Persona> persones;

    public GestioPersona() {
        persones = new ArrayList();
    }

    /**
     *
     * @param p
     * @throws NifNoValidException
     */
    @Override
    public void afegir(Persona p) throws GestioPersonaException {
        for (Persona per : persones) {
            if (per.getNif().equals(p.getNif())) {
                throw new GestioPersonaException("NIF introduit ja existent");
            }
        }

        if (!comprovaNif(p.getNif())) {
            throw new DadesPersonaNoValidesException("NIF introduit no valid");
        } else if (!comprovaEdat(p.getDataNaixement())) {
            throw new DadesPersonaNoValidesException("Data introduida no valid");
        } else {
            persones.add(p);
            System.out.println("persona afegida correctament");
        }
    }

    @Override
    public Persona cercar(String nif) throws DadesPersonaNoValidesException {
        boolean existe = false;
        Persona findPersona = null;

        if (!comprovaNif(nif)) {
            throw new DadesPersonaNoValidesException("Format del NIF introduit incorrecte");
        } else {
            for (Persona p : persones) {
                if (p.getNif().equals(nif)) {
                    existe = true;
                    findPersona = p;
                    break;
                }
            }
            if (!existe) {
                throw new DadesPersonaNoValidesException("No s'ha trobat el NIF introduit");
            }
        }
        return findPersona;
    }

    @Override
    public List<Persona> llistar() {
        return persones;
    }

    private boolean comprovaNif(String nif) {
        String letrasNif = "TRWAGMYFPDXBNJZSQVHLCKE";
        boolean correcto = false;
        String cadenaNumeros = "";

        if (nif.matches("([0-9]){7,8}[A-Z]{1}")) {
            for (int i = 0; i < nif.length() - 1; i++) {
                cadenaNumeros += nif.charAt(i);
            }

            char letraNif = (nif.charAt(nif.length() - 1));
            int posicionLetra = (Integer.parseInt(cadenaNumeros) % 23);

            if (letrasNif.charAt(posicionLetra) == letraNif) {
                correcto = true;
            }
        }
        return correcto;
    }

    private boolean comprovaEdat(LocalDate data) {
        boolean fechaCorrecta = false;
        long años = YEARS.between(data, LocalDate.now());

        if (años >= 16 && años <= 65) {
            fechaCorrecta = true;
        }
        return fechaCorrecta;
    }

}
