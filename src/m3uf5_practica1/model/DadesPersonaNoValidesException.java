package m3uf5_practica1.model;

public class DadesPersonaNoValidesException extends GestioPersonaException {

    public DadesPersonaNoValidesException(String message) {
        super(message);
    }
    
    public DadesPersonaNoValidesException(String message, Throwable cause) {
        super(message, cause);
    }

}
