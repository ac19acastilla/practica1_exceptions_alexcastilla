package m3uf5_practica1.model;

import java.time.LocalDate;


public class Persona {

    private String nif;
    private String nom;
    private LocalDate dataNaixement;

    public Persona(String nif, String nom, LocalDate dataNaixement) {
        this.nif = nif;
        this.nom = nom;
        this.dataNaixement = dataNaixement;
    }

    public String getNif() {
        return nif;
    }

    public String getNom() {
        return nom;
    }

    public LocalDate getDataNaixement() {
        return dataNaixement;
    }

    @Override
    public String toString() {
        return "nif=" + nif + ", nom=" + nom + ", dataNaixement=" + dataNaixement;
    }

    @Override
    public int hashCode() {
        return super.hashCode(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean equals(Object o) {
        boolean eq=false;
        if (this == o) {
            eq = true;
        } else if (o instanceof Persona) {
            Persona p = (Persona) o;
            eq = nif.equals(p.nif);            
        }        
        return eq;
    }
    
    
    
    
    
    

}
