package m3uf5_practica1.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import m3uf5_practica1.model.DadesPersonaNoValidesException;
import m3uf5_practica1.model.GestioPersona;
import m3uf5_practica1.model.GestioPersonaException;
import m3uf5_practica1.model.Persona;
import m3uf5_practica1.vista.PersonaGUI;

public class PersonaControl implements ActionListener {

    private static final Logger LOGGER = Logger.getLogger(PersonaControl.class.getName());
    private GestioPersona gestio;
    private PersonaGUI gui;

    public PersonaControl(GestioPersona gestio) {
        this.gestio = gestio;
        this.gui = new PersonaGUI(this);
        gui.setVisible(true);
    }

    /**
     * mètode de la interfície ActionListener, s'executa cada vegada que es
     * clica a un dels botons que tenen afegida com a listener l'instància
     * d'aquesta classe. (PersonaControl)
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        //recuperem la propietat actionCommand del botó sobre el que s'ha clicat
        String accio = e.getActionCommand();

        switch (accio) {
            case "Afegir": {
                try {
                    afegir();
                } catch (GestioPersonaException ex) {
                    Logger.getLogger(PersonaControl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            break;
            case "Cercar":
                cercar();
                break;
            case "Llistar":
                llistar();
                break;
            case "Sortir":
                System.exit(0);
        }

    }

    /**
     * accions necessàries per afegir la persona
     */
    private void afegir() throws DadesPersonaNoValidesException, GestioPersonaException {
        String resultado;
        String nif = gui.getNifAfegir();
        String nom = gui.getNomAfegir();
        String stringData = gui.getDataNaixementAfegir();
        LocalDate data = passarStringAData(stringData);
        Persona newPersona = new Persona(nif, nom, data);
        gestio.afegir(newPersona);
        resultado = "Persona afegida.";
        gui.netejarAfegir();
        LOGGER.log(Level.INFO, "Persona afegida");
        gui.setResultatAfegir(resultado);
    }

    /**
     * accions necessàries per cercar
     */
    private void cercar() {
        try {
            String nif = gui.getNifCercar();
            String personaTrobada = gestio.cercar(nif).toString();
            gui.setPersonaCercar(personaTrobada);
        } catch (GestioPersonaException ex) {
            gui.setResultatCercar(ex.getMessage());
        }
        gui.netejarCercar();
    }

    /**
     * accions necessàries per llistar
     */
    private void llistar() {
        StringBuilder personas = new StringBuilder();
        List<Persona> listaPersonas = gestio.llistar();
        int contador = 1;
        for (Persona p : listaPersonas) {
            personas.append(contador + ". " + p.toString() + "\n");
            contador++;
        }
        gui.setLlistar(personas.toString());
    }

    private LocalDate passarStringAData(String dataText) throws DadesPersonaNoValidesException {
        //array de strings que separa cada cadena delimitada por "/" y la sustituye por ESPACIOS
        String[] dataIntroducidaSeparada = dataText.replaceAll(" ", "").split("/");
        LocalDate ldFechaIntroducida;
        try {
            ldFechaIntroducida = LocalDate.of(Integer.parseInt(dataIntroducidaSeparada[0]), Integer.parseInt(dataIntroducidaSeparada[1]), Integer.parseInt(dataIntroducidaSeparada[2]));
        } catch (Exception ex) {
            throw new DadesPersonaNoValidesException("Data introduida no es valida, utilitza el format: any/mes/dia");
        }
        return ldFechaIntroducida;
    }

}
