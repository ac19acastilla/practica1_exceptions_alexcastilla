package m3uf5_practica1;

import java.io.FileInputStream;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import m3uf5_practica1.control.PersonaControl;
import m3uf5_practica1.model.GestioPersona;

public class MainClass {

    public static void main(String[] args) {

        try {
            LogManager.getLogManager().readConfiguration(new FileInputStream("./log/archivoLog.properties"));
        } catch (Exception ex) {
            Logger.getLogger(MainClass.class.getName()).log(Level.SEVERE, null, ex);
        }

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                PersonaControl control = new PersonaControl(new GestioPersona());
            }
        });
    }

}
